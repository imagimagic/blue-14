import sys
from pathlib import Path
from typing import Final, Iterable, Literal

import easygui
import cv2 as cv
import numpy as np
import pymsgbox


def select_image(prompt: str) -> tuple[str, np.ndarray]:
    if (path_str := easygui.fileopenbox(title=prompt)) is None:
        exit(0)

    file_path = Path(path_str)

    if (image := cv.imread(str(file_path))) is None:
        pymsgbox.alert(title='Error', text=f'Could not open file:\n{file_path}', icon=pymsgbox.WARNING)
        exit(1)

    return path_str, image


TEMPLATES_PATH: Final[Path] = Path('templates')

TEMPLATES_EXTENSION: Final[str] = '.png'


def load_digit_templates() -> dict[int, np.ndarray]:
    return {
        digit: cv.imread(str(TEMPLATES_PATH / f'{digit}{TEMPLATES_EXTENSION}'), cv.IMREAD_GRAYSCALE)
        for digit in range(10)
    }


def load_win_template() -> np.ndarray:
    return cv.imread(str(TEMPLATES_PATH / f'win{TEMPLATES_EXTENSION}'), cv.IMREAD_GRAYSCALE)


def load_templates() -> dict[int | Literal['win'], np.ndarray]:
    return load_digit_templates() | {'win': load_win_template()}


MATCH_THRESHOLD: Final[float] = 0.01


def top_matches(
        match_result: np.ndarray,
        limit: int,
        threshold: float = MATCH_THRESHOLD,
) -> list[tuple[int, int]]:
    loc = np.where(match_result <= threshold)
    return [(x, y) for (x, y) in list(zip(*loc[::-1]))[:limit]]


def match_all(
        image_gray: np.ndarray,
        template: np.ndarray,
        limit: int,
        threshold: float = MATCH_THRESHOLD,
) -> list[tuple[tuple[int, int], tuple[int, int]]]:
    assert len(image_gray.shape) == 2
    assert len(template.shape) == 2

    match_result = cv.matchTemplate(image_gray, template, method=cv.TM_SQDIFF_NORMED)
    best_matches = top_matches(match_result, limit, threshold)

    w: int = template.shape[1]
    h: int = template.shape[0]

    return [((x, y), (x + w, y + h)) for x, y in best_matches]


def crop(
        image_gray: np.ndarray,
        top_left: tuple[int, int],
        bottom_right: tuple[int, int]
) -> np.ndarray:
    assert len(image_gray.shape) == 2

    x1, y1 = top_left
    x2, y2 = bottom_right

    return image_gray[y1:y2, x1:x2]


def parse_time(matched_digits: Iterable[tuple[int, tuple[int, int], tuple[int, int]]]) -> str | None:
    matched_digits = sorted(matched_digits, key=lambda it: it[1][0])
    digits = [digit for digit, _, _ in matched_digits]
    digit_pairs = list(zip(digits[::2], digits[1::2]))

    match digit_pairs:
        case [hh, mm, ss]:
            return f'{hh[0]}{hh[1]} Hours {mm[0]}{mm[1]} Minutes {ss[0]}{ss[1]} Seconds'
        case [mm, ss]:
            return f'{mm[0]}{mm[1]} Minutes {ss[0]}{ss[1]} Seconds'
        case _:
            return None


def find_match_length_digits(
        image_gray: np.ndarray,
        templates: dict[int | Literal['win'], np.ndarray]
) -> list[tuple[int, tuple[int, int], tuple[int, int]]] | None:
    win_texts = match_all(image_gray, templates['win'], limit=1)
    if not win_texts:
        return None

    (origin_x1, origin_y1), (origin_x2, origin_y2) = win_texts[0]
    origin = np.array((origin_x1, origin_y1))

    sub_image_gray = crop(image_gray, (origin_x1, origin_y1), (origin_x1 + 128, origin_y2))

    matched_digits = []

    for digit in range(10):
        matches = match_all(sub_image_gray, templates[digit], limit=10)
        for bounding_box in matches:
            (x1, y1), (x2, y2) = origin + bounding_box[0], origin + bounding_box[1]
            matched_digits.append((digit, (x1, y1), (x2, y2)))

    return matched_digits


def run_interactive() -> int:
    image_path, image = select_image('Open Game Results')
    if image is None:
        return 1

    image_gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    templates = load_templates()

    matched_digits = find_match_length_digits(image_gray, templates)
    for digit, top_left, bottom_right in matched_digits:
        image = cv.rectangle(
            image, np.array([-1, -1]) + top_left, np.array([1, 1]) + bottom_right,
            color=(32, 255, 192), thickness=1
        )
        image = cv.putText(
            image, f'{digit}', np.array([0, -7]) + top_left,
            fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=0.4, color=(32, 255, 192), thickness=1
        )

    time_str = parse_time(matched_digits) or 'Could not parse time'
    print(time_str)
    image = cv.putText(
        image, time_str, (10, 15),
        fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=0.4, color=(32, 255, 192), thickness=1
    )

    cv.imshow(image_path, image)
    cv.waitKey(0)
    cv.destroyAllWindows()


def main(args: list[str]) -> int:
    if not args:
        return run_interactive()

    templates = load_templates()
    if len(args) == 1 and Path(args[0]).is_dir():
        args = list(map(str, Path(args[0]).iterdir()))

    for image_path in args:
        print(f'{image_path} - ', end='')
        if (image := cv.imread(image_path, cv.IMREAD_GRAYSCALE)) is None:
            print(f'could not read image')
            continue

        print(parse_time(find_match_length_digits(image, templates)) or 'could not parse match length')

    return 0


if __name__ == '__main__':
    exit(main(sys.argv[1:]))
